## Right approach to merging branches that are dependent

### Getting started

#### Create branch A from develop
```shell
git checkout -b branchA
```

#### Merge request of branch A. `branchA -> develop`
![branchA2develop.png](images/branchA2develop.png)


#### Let's assume we have new task B and it needs changes from task A (branchA).
#### Create branch B from branch A
```shell
git checkout -b branchB
```

#### Merge request of branch B. `branchB -> branchA`
![branchB2branchA.png](images/branchB2branchA.png)


It is not right approach to merge branchB to branchA.Both branches should be merged into develop. 
When we merge `branchA` into `develop`, target in merge request (`branchB` into `branchA`) will automatically be changed into develop.

#### Let's test now. First i will merge `branchA` into `develop`.
![branchA2developmerged.png](images/branchA2developmerged.png)

#### Following merge request target is updated automatically.
![branchB2develop.png](images/branchB2develop.png)



### Summary
#### Each task main branches should be merged into develop rather than merging them into dependent branches.